const express = require("express");
const router = express.Router();

const ProductServices = require("../Services/ProductService");
const ValidatorHandler = require("../middlewares/ValidatorHandler");
const {
  CreateProductSchema,
  UpdateProductScehma,
  GetProductSchema,
} = require("../schemas/ProductScehma");

const Services = new ProductServices();

//Query params
router.get("/", async (req, res) => {
  const products = await Services.find();
  res.status(200).json(products);
});

//params
router.get(
  "/:id",
  ValidatorHandler(GetProductSchema, "params"),
  async (req, res, next) => {
    console.log("pasandodel middleware");
    try {
      const { id } = req.params;
      const product = await Services.findOne(id);
      res.json(product);
    } catch (error) {
      next(error);
    }
  }
);

// Json body (Created always with post)
router.post(
  "/",
  express.json(),
  ValidatorHandler(CreateProductSchema, "body"),
  async (req, res) => {
    const body = req.body;
    const newProduct = await Services.create(body);
    res.status(201).json(newProduct);
  }
);

// Update (Put or patch)
router.put(
  "/:id",
  express.json(),
  ValidatorHandler(GetProductSchema, "params"),
  ValidatorHandler(UpdateProductScehma, "body"),
  async (req, res) => {
    try {
      const { id } = req.params;
      const body = req.body;
      const newProduct = await Services.update(id, body);
      res.json(newProduct);
    } catch (error) {
      res.status(404).json({
        message: error.message,
      });
    }
  }
);

// Delete
router.delete(
  "/:id",
  ValidatorHandler(GetProductSchema, "params"),
  async (req, res) => {
    const { id } = req.params;
    const deleteProduct = await Services.delete(id);
    res.json(deleteProduct);
  }
);

module.exports = router;
