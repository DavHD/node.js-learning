const express = require("express");
const routerApi = require("./routes")
const cors = require('cors')

const { logError, errorHandler, boomErrorHandler } = require('./middlewares/ErrorHandler')

const app = express();
const port = 3000;

// whit the next configuration we can let the access to the api, with the followings origins 
const whitelist = ["http://127.0.0.1:5500"]
const options = {
  origin: (origin, callback )=>{
    if(whitelist.includes(origin)){
      callback(null,true)
    }else{
      callback(new Error('not allowed'))
    }
  } 
}

app.use(cors(options));

routerApi(app);

app.use(logError);
app.use(boomErrorHandler);
app.use(errorHandler);

app.listen(port, () => {
  console.log("Mi puerto es " + port);
});

